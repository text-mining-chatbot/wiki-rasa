## default
* default
  - action_default_fallback

## greet
* greet
  - utter_greet

## say goodbye
* goodbye
  - utter_goodbye

## chitchat
* chitchat
  - utter_no_chitchat

## nationality
* nationality{"physicist": "albert einstein"}
  - action_search_nationality
  - action_utter_nationality

## birthdate
* birthdate{"physicist": "albert einstein"}
  - action_search_birthdate
  - action_utter_birthdate

## day_of_death
* day_of_death{"physicist": "albert einstein"}
  - action_search_day_of_death
  - action_utter_day_of_death

## place_of_death
* place_of_death{"physicist": "albert einstein"}
  - action_search_place_of_death
  - action_utter_place_of_death

## is_alive
* is_alive{"physicist": "albert einstein"}
  - action_search_is_alive
  - action_utter_is_alive

## spouse
* spouse{"physicist": "albert einstein"}
  - action_search_spouse
  - action_utter_spouse

## primary_education
* primary_education{"physicist": "albert einstein"}
  - action_search_primary_education
  - action_utter_primary_education

## university
* university{"physicist": "albert einstein"}
  - action_search_university
  - action_utter_university

## area_of_research
* area_of_research{"physicist": "albert einstein"}
  - action_search_area_of_research
  - action_utter_area_of_research

## workplace
* workplace{"physicist": "albert einstein"}
  - action_search_workplace
  - action_utter_workplace

## awards
* awards{"physicist": "albert einstein"}
  - action_search_awards
  - action_utter_awards
