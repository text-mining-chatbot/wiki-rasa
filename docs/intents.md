<!--Personendaten-->
## intent:birthplace
- where was [Albert Einstein](Physicist) born?
- where is [Albert Einstein](Physicist) from?
- what is [Albert Einstein](Physicist) birthplace?
- birthplace of [Albert Einstein](Physicist)?

## intent:birthdate
- when was [Albert Einstein](Physicist) born?
- what is [Albert Einstein](Physicist) birthdate?

## intent:day_of_death
- when did [Albert Einstein](Physicist) die?
- what is [Albert Einstein](Physicist) date of death?

## intent:place_of_death
- where did [Albert Einstein](Physicist) die?
- where is [Albert Einstein](Physicist) place of death?

## intent:is_alive
- is [Albert Einstein](Physicist) still alive?
- is [Albert Einstein](Physicist) dead?

## intent:spouse
- which person was [Albert Einstein](Physicist) married to?
- was [Albert Einstein](Physicist) married?
- who was [Albert Einstein](Physicist) partner?

## intent:parents
- who where [Albert Einstein](Physicist) parents?

## intent:children
- did [Albert Einstein](Physicist) have children?
- who where [Albert Einstein](Physicist) children?

<!--professional life-->
## intent:primary_education
- where did [Albert Einstein](Physicist) go to school?
- which school did [Albert Einstein](Physicist) go to?

## intent:university
- which university did [Albert Einstein](Physicist) go to?
- which university did [Albert Einstein](Physicist) attend?
- where did [Albert Einstein](Physicist) study?

## intent:area_of_research
- what was [Albert Einstein](Physicist) area of research?
- what was [Albert Einstein](Physicist) field of interest?
- what did [Albert Einstein](Physicist) research?

## intent:workplace
- where did [Albert Einstein](Physicist) work?
- where was [Albert Einstein](Physicist) workplace?
- what institute did [Albert Einstein](Physicist) work at?
- what university did [Albert Einstein](Physicist) work at?

## intent:awards
- what awards did [Albert Einstein](Physicist) win?
- what are [Albert Einstein](Physicist) awards?
- did [Albert Einstein](Physicist) win the nobel price?






Titel/Nobelpreise?  
Forschungsbereich?  
Wann wurde er/sie geboren?  


