#!/usr/bin/env bash

# Install conda
wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
/bin/bash ~/miniconda.sh -b -p /opt/conda && \
rm ~/miniconda.sh && \
/opt/conda/bin/conda clean -tipsy && \
ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
echo "conda activate base" >> ~/.bashrc

# Create conda env
source ~/.bashrc && \
conda create -y -n spcy python=3 && \
conda activate spcy && \
pip install spacy && \
python -m spacy download en && \
conda deactivate

sudo apt-get update && sudo apt-get dist-upgrade -y && \
sudo apt install dirmngr -y --install-recommends && \
sudo apt install software-properties-common apt-transport-https -y && \
sudo apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF' && \
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/debian stretch-cran35/' && \
sudo apt-get update && \
sudo apt-get install -y r-base-dev && \
sudo apt-get install --no-install-recommends libcurl4-openssl-dev libssl-dev libxml2-dev -y 