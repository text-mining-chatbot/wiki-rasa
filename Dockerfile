FROM rocker/r-ver:3.5.1

# Get package dependencys
RUN apt-get update && apt-get install -y --no-install-recommends libxml2-dev \
libssl-dev \
libcurl4-openssl-dev \
wget \
bzip2 \
curl \
git && \
apt-get clean && \
rm -rf /var/lib/apt/lists/*

# Install miniconda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
/bin/bash ~/miniconda.sh -b -p /opt/conda && \
rm ~/miniconda.sh && \
/opt/conda/bin/conda clean -tipsy && \
ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
echo "conda activate base" >> ~/.bashrc

# Copy conda env setup script
# This is required as conda activate commands will not work during docker build,
# unless called from within a bash script
COPY docker/create_spcy.sh /setup/create_spcy.sh

# Create miniconda environment
RUN bash /setup/create_spcy.sh

# Copy package list
COPY processing/packages.list /setup/packages.list

# Install R Packages
RUN R -e "install.packages(readLines('/setup/packages.list'))"

# Copy the rasa data over
COPY rasa/* /app/rasa/
COPY docker/create_rasa.sh /setup/create_rasa.sh
COPY docker/train_rasa.sh /setup/train_rasa.sh

# Install rasa and requirements
RUN bash /setup/create_rasa.sh

# Copy wikiproc package, needs to be created by the build script
COPY wikiproc_0.0.0.9000.tar.gz /setup/wikiproc_0.0.0.9000.tar.gz

# Install wikiproc package
RUN R CMD INSTALL /setup/wikiproc_0.0.0.9000.tar.gz

# Copy R script and bash wrapper. Also readme, as its currently needed to find root directory
COPY processing/script/master.R /app/script/master.R
COPY docker/master.sh /app/script/master.sh
COPY README.md /app/README.md

# Optionally: Copy cache to speed up data processing
# COPY data/articles.RDS /app/data/articles.RDS
# COPY data/annotations/* /app/data/annotations/

RUN bash /app/script/master.sh

# Train the rasa bot
RUN bash /setup/train_rasa.sh

# Clean up stuff we won't need in production
RUN rm -rf /setup/* && \
rmdir /setup/

# Clean up cached data if copied
# RUN rm -rf /app/data/* && \
# rmdir /app/data/
 
COPY docker/docker-entrypoint.sh /app/docker-entrypoint.sh
ENTRYPOINT ["/app/docker-entrypoint.sh"]
# CMD [ "/bin/bash" ]