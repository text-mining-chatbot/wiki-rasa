library(ggplot2)

## First of we get the spouses from all the infoboxes we got

spouses <- sapply(articles$Text, function(x) {
  info_box <- wikiproc:::get_infobox(x)
  if(!is.na(info_box) && "Spouse(s)" %in% info_box$Desc) {
    return(info_box[info_box$Desc == "Spouse(s)",]$Content)
  } else {
    return(NA)
  }
})

# Remove names

spouses <- unname(spouses)

# Remove everything in parentheses and square brackets

spouses <- gsub("\\s*\\([^\\)]+\\)", " ", spouses)
spouses <- gsub("\\[.*?\\]", "", spouses)

# We bind this onto the results data frame now to subset

results_with_infobox <- results[, c("name", "spouse")]
results_with_infobox$spouses_infobox <- spouses

# We filter only for thoses were we got a value in both columns

results_with_both <- results_with_infobox[!is.na(results_with_infobox$spouse) & !is.na(results_with_infobox$spouses_infobox),]

results_with_both$distance <- apply(results_with_both, 1, function(x) {
  adist(x[2], x[3], partial = TRUE)[1,1]
})

precision <- nrow(results_with_both[results_with_both$distance <= 5,]) / nrow(results_with_both)

# Recall is a bit more difficult
# First of we check for all articles if they even contain the word marry

results_with_infobox$has_married <- apply(articles, 1, function(x) {
  cleaned_text <- wikiproc::clean_html(x[4])
  annotation <- wikiproc::create_annotations(cleaned_text, x[2], x[3], data.dir = data_dir)
  tokens <- cleanNLP::cnlp_get_token(annotation)
  if ("marry" %in% tokens$lemma) {
    return(TRUE)
  } else {
    return(FALSE)
  }
})

recall_high <- nrow(results_with_infobox[!is.na(results_with_infobox$spouse),]) / nrow(results_with_infobox[results_with_infobox$has_married,])

# Another possibility to calculate stuff

# Get the ones we know were married and calculate recall only for those

results_with_infobox <- results_with_infobox[!is.na(results_with_infobox$spouses_infobox),]

recall_low <- nrow(results_with_infobox[!is.na(results_with_infobox$spouse),]) / nrow(results_with_infobox[results_with_infobox$has_married,])


eval_res <- data.frame(Parameter = c("Precision", "Recall (Low Estimate)", "Recall (High Estimate)"),
                       Value = c(precision, recall_low, recall_high),
                       stringsAsFactors = FALSE)


p <- ggplot(eval_res, aes(x = Parameter, y = Value))
p <- p + geom_bar(position = "dodge", stat = "identity", fill = "blue", width = 0.5)
p <- p + geom_text(aes(label = sprintf("%0.2f", round(Value, digits = 2))), vjust = 0, nudge_y = 0.01)
p <- p + theme_minimal()
p <- p + theme(axis.title.x = element_blank(), axis.title.y = element_blank())
p

ggsave("spouse_eval.png", path = "plots", width = 5, height = 4, units = "in")
