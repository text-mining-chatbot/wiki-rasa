#!/usr/bin/env bash
source ~/.bashrc 
conda activate rasa_env
cd /app/rasa
make train