from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet
import os
import csv
import re

"""

Custom Actions to get Information about 

Schema for Actions

class ActionSearchInfo(Action):
    Starts Search Action for an Information, depending on
    found intent (Intent 'nationality' -> 'ActionSearchNationality')
    - Obtains slot value of recognized physicist entity
    - Iterates over 'data.tsv' to find demanded info for given physicist
    - Stores value of info in its slot

class ActionUtterInfo(Action): 
    Starts Utterance Action for an Information
    - Obtains slot value for recognized physiscist entity and info value
    from Search-Action
    - Prints Bot utterance containing the above to the console
    
"""

class ActionDefaultFallback(Action):
    def name(self):
        return 'action_default_fallback'

    def run(self, dispatcher, tracker, domain):
        dispatcher.utter_message("I didn't understand  that. Please enter another question!")
        return []


#
# Nationality
#
class ActionSearchNationality(Action):
    def name(self):
        return 'action_search_nationality'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_nationality = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_nationality = row['nationality']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
            
        return [SlotSet('nationality', actual_nationality
            if actual_nationality is not None and actual_nationality is not ""
            else "not known")]
        

class ActionUtterNationality(Action):
    def name(self):
        return 'action_utter_nationality'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        nationality = tracker.get_slot('nationality')
        if person is not None:
           dispatcher.utter_message("Nationality of {} is {}.".format(person, nationality))
        return []


#
# Birthdate
#
class ActionSearchBirthdate(Action):
    def name(self):
        return 'action_search_birthdate'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_birthdate = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_birthdate = row['birthdate']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('birthdate', actual_birthdate
            if actual_birthdate is not None and actual_birthdate is not ""
            else "not known")]


class ActionUtterBirthdate(Action):
    def name(self):
        return 'action_utter_birthdate'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        birthdate = tracker.get_slot('birthdate')
        if person is not None:
           dispatcher.utter_message("Birthdate of {} is {}.".format(person, birthdate))
        return []


#
# Day of death
#
class ActionSearchDayOfDeath(Action):
    def name(self):
        return 'action_search_day_of_death'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_day_of_death = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_day_of_death = row['day_of_death']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('day_of_death', actual_day_of_death 
            if actual_day_of_death is not None and actual_day_of_death is not ""
            else "not known")]
        
        
class ActionUtterDayOfDeath(Action):
    def name(self):
        return 'action_utter_day_of_death'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        day_of_death = tracker.get_slot('day_of_death')
        if person is not None:
           dispatcher.utter_message("The day of death of {} is {}.".format(person, day_of_death))
        return []


#
# Place of death
#
class ActionSearchPlaceOfDeath(Action):
    def name(self):
        return 'action_search_place_of_death'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_place_of_death = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_place_of_death = row['place_of_death']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('place_of_death', actual_place_of_death
            if actual_place_of_death is not None and actual_place_of_death is not ""
            else "not known")]
        
        
class ActionUtterPlaceOfDeath(Action):
    def name(self):
        return 'action_utter_place_of_death'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        place_of_death = tracker.get_slot('place_of_death')
        if person is not None:
           dispatcher.utter_message("The place of death of {} is {}.".format(person, place_of_death))
        return []


#
# Is alive
#
class ActionSearchIsAlive(Action):
    def name(self):
        return 'action_search_is_alive'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_is_alive = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_is_alive = row['is_alive']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('is_alive', actual_is_alive 
            if actual_is_alive is not None and actual_is_alive is not ""
            else "not known")]
        
        
class ActionUtterIsAlive(Action):
    def name(self):
        return 'action_utter_is_alive'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        is_alive = tracker.get_slot('is_alive')
        if person is not None:
           dispatcher.utter_message("{} is {}.".format(person, is_alive))
        return []


#
# spouse
#
class ActionSearchSpouse(Action):
    def name(self):
        return 'action_search_spouse'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_spouse = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_spouse = row['spouse']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('spouse', actual_spouse 
            if actual_spouse is not None and actual_spouse is not "" 
            else "not known")]
        
        
class ActionUtterSpouse(Action):
    def name(self):
        return 'action_utter_spouse'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        spouse = tracker.get_slot('spouse')
        dispatcher.utter_message("The spouse of {} is {}.".format(person, spouse))
        return []


#
# Primary Education
#
class ActionSearchPrimaryEducation(Action):
    def name(self):
        return 'action_search_primary_education'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_primary_education = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_primary_education = row['primary_education']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('primary_education', actual_primary_education 
            if actual_primary_education is not None and actual_primary_education is not ""
            else "not known")]
        
        
class ActionUtterPrimaryEducation(Action):
    def name(self):
        return 'action_utter_primary_education'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        primary_education = tracker.get_slot('primary_education')
        if person is not None:
           dispatcher.utter_message("The primary education of {} is {}.".format(person, primary_education))
        return []


#
# University
#
class ActionSearchUniversity(Action):
    def name(self):
        return 'action_search_university'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_university = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_university = row['university']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('university', actual_university
            if actual_university is not None and actual_university is not "" 
            else "not known")]
        
        
class ActionUtterUniversity(Action):
    def name(self):
        return 'action_utter_university'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        university = tracker.get_slot('university')
        if person is not None:
           dispatcher.utter_message("The University of {} is {}.".format(person, university))
        return []


#
# Area of Research
#
class ActionSearchAreaOfResearch(Action):
    def name(self):
        return 'action_search_area_of_research'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_area_of_research = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_area_of_research = row['area_of_research']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('area_of_research', actual_area_of_research
            if actual_area_of_research is not None and actual_area_of_research is not "" 
            else "not known")]
        
        
class ActionUtterAreaOfResearch(Action):
    def name(self):
        return 'action_utter_area_of_research'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        area_of_research = tracker.get_slot('area_of_research')
        if person is not None:
           dispatcher.utter_message("{} did research in {}.".format(person, area_of_research))
        return []


#
# Workplace
#
class ActionSearchWorkplace(Action):
    def name(self):
        return 'action_search_workplace'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_workplace = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_workplace = row['workplace']
        else:
            dispatcher.utter_message("Warning: Data not generated!")
        
        return [SlotSet('workplace', actual_workplace 
            if actual_workplace is not None and actual_workplace is not ""
            else "not known")]
        
        
class ActionUtterWorkplace(Action):
    def name(self):
        return 'action_utter_workplace'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        workplace = tracker.get_slot('workplace')
        if person is not None:
           dispatcher.utter_message("The Workplace of {} is {}.".format(person, workplace))
        return []


#
# Awards
#
class ActionSearchAwards(Action):
    def name(self):
        return 'action_search_awards'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        if person is "" or person is None:
            dispatcher.utter_message("Error: Physicist entity not found! Please enter another question.")
            return []
        else:
            name_regex = re.compile(person, re.IGNORECASE)
        actual_awards = None
        if os.path.isfile('data.tsv'):
            with open('data.tsv') as csvfile:
                spamreader = csv.DictReader(csvfile, delimiter='\t')
                for row in spamreader:
                    if name_regex.match(row['name']):
                        actual_awards = row['awards']
        else:

            dispatcher.utter_message("Warning: Data not generated!")
            
        return [SlotSet('awards', actual_awards 
            if actual_awards is not None and actual_awards is not ""
            else "not known")]
        
        
class ActionUtterAwards(Action):
    def name(self):
        return 'action_utter_awards'

    def run(self, dispatcher, tracker, domain):
        person = tracker.get_slot('physicist')
        awards = tracker.get_slot('awards')
        if person is not None:
           dispatcher.utter_message("The Awards of {} are {}".format(person, awards))
        return []

