#!/usr/bin/env bash
source ~/.bashrc 
cd /app/rasa 
conda activate rasa_env 
make run