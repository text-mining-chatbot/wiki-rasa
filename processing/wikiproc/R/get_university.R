#!/usr/bin/env Rscript

## Authors: Leonard

#' Function to extract academic affiliation from text
#' 
#' @param annotation annotations object from cleanNLP's createAnnotations() function.
#'
#' @return string with all found Academia organizations
#' @export
get_university <- function(annotation) {
  # Extracts the entitys 
  
  entities <- cleanNLP::cnlp_get_entity(annotation)
  
  # Extracts only the organization entitys

  entities_org <- entities[entities$entity_type == "ORG", ]
  char_org <- entities_org$entity
  
  ## Find relevant entities
  # Define keywords
  
  to_match <- c("Academy", "University", "Institute", "Department", "Research Centre")
  
  # Find matching entities

  string_matched <- grep(paste(to_match, collapse = "|"), char_org) %>%
    entities_org$entity[.] %>%
    unique(.)
  
  # Return NA if we found nothing 

  if (length(string_matched) == 0) {
    return(NA)
  }

  # Return result if we only found one

  if(length(string_matched)== 1){
    return(string_matched)
  }
  
  ## Duplicates
  # Creat Matrix of levistein distances to eleminate duplicates
  
  string_dup <- sapply(string_matched, function(x) {
    sapply(string_matched, function(y) {
      adist(x, y, partial = TRUE)
    })
  })
  
  r <- as.data.frame(string_dup)
  
  # Return a table of logic vector were the distance is too low -> dublicate
  
  res <- sapply(r, function(x) {
    x <= 5 & x != 0
  })
  
  # Gives list with potential duplicates
  
  str <- sapply(seq_along(r), function(x) {
    colnames(r)[res[x, ]]
  })

  # Eliminate possible duplicates  

  dup <- unlist(str, use.names = FALSE)
  result <- string_matched
  result <- result[!result %in% dup]
  
  # If we got nothing now (for some reason) return NA

  if (length(result) == 0) {
    return(NA)
  }
  
  # Bind the results together and return them

  result <- paste(result, collapse = ", ")
}
