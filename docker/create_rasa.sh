#!/usr/bin/env bash
source ~/.bashrc
conda create -y -n rasa_env python=3.6.7
conda activate rasa_env
pip install spacy
pip install rasa_nlu
pip install rasa_core
pip install sklearn_crfsuite
python -m spacy download en_core_web_md
python -m spacy link en_core_web_md en