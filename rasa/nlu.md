## intent:greet
- hey there
- hi

## intent:goodbye
- bye
- goodbye

## intent:nationality
- what nation is [Albert Einstein](physicist) from
- what nationality does [Albert Einstein](physicist) have
- where is [Galileo Galilei](physicist) from

## intent:birthdate
- when was [Albert Einstein](physicist) born
- what is [Galileo Galilei](physicist) birthdate

## intent:day_of_death
- when did [Albert Einstein](physicist) die
- what is [Galileo Galilei](physicist) date of death

## intent:place_of_death
- where did [Albert Einstein](physicist) die
- where is [Galileo Galilei](physicist) place of death

## intent:is_alive
- is [Albert Einstein](physicist) still alive
- is [Galileo Galilei](physicist) dead

## intent:spouse
- to whom was [Albert Einstein](physicist) married
- who was [Galileo Galilei](physicist) spouse
- who was [Albert Einstein](physicist) wife
- who was [Galileo Galilei](physicist) husband

## intent:primary_education
- where did [Galileo Galilei](physicist) go to school
- which school did [Albert Einstein](physicist) go to

## intent:university
- which university did [Galileo Galilei](physicist) go to
- which university did [Albert Einstein](physicist) attend
- where did [Galileo Galilei](physicist) study

## intent:area_of_research
- what was [Albert Einstein](physicist) area of research
- what was [Galileo Galilei](physicist) field of interest
- what did [Albert Einstein](physicist) research

## intent:workplace
- where did [Galileo Galilei](physicist) work
- where was [Albert Einstein](physicist) workplace
- what institute did [Galileo Galilei](physicist) work at
- what university did [Albert Einstein](physicist) work at

## intent:awards
- what awards did [Galileo Galilei](physicist) win
- what are [Albert Einstein](physicist) awards
- did [Galileo Galilei](physicist) win the nobel price

## intent:chitchat
- can you share your boss with me?
- i want to get to know your owner
- i want to know the company which designed you
- i want to know the company which generated you
- i want to know the company which invented you
- i want to know who invented you
- May I ask who invented you?
- please tell me the company who created you
- please tell me who created you
- tell me more about your creators
- tell me more about your founders
- Ahoy matey how are you?
- are you alright
- are you having a good day
- Are you ok?
- are you okay
- Do you feel good?
- how are things going
- how are things with you?
- How are things?
- how are you
- how are you doing
- how are you doing this morning
- how are you feeling
- how are you today
- How are you?
- How is the weather today?
- What's the weather like?
- How is the weather?
- What is the weather at your place?
- Do you have good weather?
- Is it raining?
- What's it like out there?
- Is it hot or cold?
- Beautiful day, isn't it?
- What's the weather forecast?
- Is it quite breezy outside?
