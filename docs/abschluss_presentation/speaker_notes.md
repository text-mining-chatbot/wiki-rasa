# Recap - Überblick über Thema/Aufgabe

Rasa ist ein Python-Framework für die Implementation von Chatbots im Bereich Conversational AI. ([Definition](https://www.iotforall.com/what-is-conversational-ai/) Conversational AI: A set of technologies that enable computers to simulate real conversations). Chatbots sollen dabei konkret die Funktion von ständig verfügbaren "Gesprächspartnern" übernehmen, die auf einer Website Auskünfte oder Anleitungen geben. Daraus ergeben sich verschiedene Vorteile: Für Kunden ist rund um die Uhr ein Gesprächsservice verfügbar. Außerdem verbuchen Unternehmen, die Chatbots nutzen, Einsparungen im Servicepersonalbereich.
* Funktionsweise Rasa:
  - NLU understands the user’s message based on your previous training data:
    - Intent classification: Interpreting meaning based on predefined intents (Example: Please send the confirmation to amy@example.com is a provide_email intent with 93% confidence)
    - Entity extraction: Recognizing structured data (Example: amy@example.com is an email)
  - Core decides what happens next in this conversation. It’s machine learning-based dialogue management predicts the next best action based on the input from NLU, the conversation history and your training data. (Example: Core has a confidence of 87% that ask_primary_change is the next best action to confirm with the user if they want to change their primary contact information.)

Mit Core können zudem sogenannte [Custom Actions](https://rasa.com/docs/core/customactions/) ausgeführt werden. Dabei kann über einen Endpoint beliebiger Code durch den Bot ausgeführt werden.


**Forschungsfrage:** Der Bot braucht _Wissen_ für die Reaktion auf erkannte Intents des Users.
  * Kann dieses Wissen aus Texten akquiriert werden?
  * Kann dies automatisch mit Textquellen durchgeführt werden?
    (Beispielfragen: Wer war Albert Einstein? Was hat Albert Einstein erfunden? Wo hat er gelebt?)


# Lösungsansatz

### Prototyp und Intents

Zur Beantwortung der Forschungsfrage haben wir zunächst einen Bot-Prototypen aufgesetzt. Dafür war der Download von Rasa NLU und Rasa Core über pip oder conda nötig. Außerdem ist für Rasa eine Umgebung mit Python 3.6 notwendig. Dafür wurde miniconda genutzt. Beim Konfigurieren des Bots war festzustellen, dass für das Training der NLU-Komponente aus der Kommunikation des Users zu extrahierende Entitäten und Intents festgelegt werden müssen. Aus diesem Grund legten wurden die ersten Intents festgelegt, mit denen der Prototyp trainiert werden sollte.
```
  - birth - "Where and when was $physicist born?"
  - isAlive - "Is $physicist still alive?"
  - education - "Where did $physicist go to school?"
  - researchArea - "What did $physicist discover?"
  - hasNobelPrize - "Did $physicist win the Nobel Prize?"

```
Bei der Nutzung des Bots fiel schnell auf, dass für die Beantwotung der Fragen eine Wissensbasis der Form (physicist x, intent y) benötigt würde.

| `data`   | intent1 | intent2 | ... |
|----------|---------|---------|-----|
|physicist1| _data_  | _data_  | ... |
|physicist1| _data_  | _data_  | ... |
|   ...    |  ...    |  ...    | ... |

### Datengewinn

Daher wurde der Ansatz gewählt _zu einer vorbestimmten Menge an Intents mittels Text Mining-Verfahren das nötige Wissen zu akquirieren_. Zu der gegebenen Auswahl an Intents wurden, wie in der Aufgabenstellung gefordert, Wikipedia-Artikel beschafft. Dazu wurde auf den Artikel [List of physicists](https://en.wikipedia.org/wiki/List_of_physicists) zurückgegriffen, der zu dem Zeitpunkt Verlinkungen auf 982 Artikel zu berühmten Physikern enthielt. Das Scrapping wird mittels eines R-Skriptes durchgeführt, das das R-Paket 'WikipediR' nutzt. Anschließend liegen in einem R-Dataframe alle Artikel als HTML vor

```
#!/usr/bin/env Rscript
...
page <- xml2::read_html("https://en.wikipedia.org/wiki/List_of_physicists")
...
article <- WikipediR::page_content("en", "wikipedia", page_name = x, as_wikitext = FALSE)
...
```

### Processing

Um aus den gewonnenen Wikipedia-Artikeln das nötige Wissen für den Bot zu generieren, wurde ein eigenes R-Paket erstellt: `processing/wikiproc`. Das Paket enthält zunächst einmal ein Skript zum Entfernen von HTML-Tags und sonstigen Formatierungsbestandteilen des Textes: `clean_html.R` Mithilfe dieses Skriptes kann entsprechend dem Forschungsfeld 'Text Mining' auf _natürlichsprachigem Text_ gearbeitet werden.
Weiterhin enthält das Paket _ein Skript pro Intent_, um mithilfe von auf den Intent zugeschnittenen Verfahren das gesuchte Wissen aus den Artikeln zu extrahieren.

```
wikiproc
├── DESCRIPTION
├── NAMESPACE
├── R
│   ├── clean_html.R
│   ├── get_awards.R
│   ├── get_birthdate.R
│   ├── get_birthplace.R
│   ├── get_data.R
│   ├── get_spouse.R
│   └── get_university.R
└── tests
```

### Processing - Beispiele

`get_birthdate.R`:
Für die Gewinnug des Geburtsdatums aus einem Text werden zunächst durch Nutzung des Paketes 'cleanNLP.R' alles DATE-Entities aus dem Text gewonnen. Davon wird die erste erkannte Entität genutzt.
Mithilfe von Regex wird zudem ein evtl über '-' anhängendes Todesdatum abgeschnitten sowie Klammern und Whitespaces entfernt.

`get_spouse.R`:
Der Ehepartner wird über NER und Patterns aus dem jeweiligen Text extrahiert. Die Patterns sind Vektoren aus POS-Tags und Wildcards für die Position der gesuchten Entität davor oder dahinter, dem Token 'marry' und Wildcards für die Position der gesuchten Entität davor oder dahinter. Mit diesen Patterns und der Menge aller PERSON Entities aus dem jeweiligen Text wird die Pattern-Matching-Funktion in 'utils.R' aufgerufen.

## Architektur

Die Extraktion des Daten mittels Skripten aus 'wikiproc' wird über ein zentrales Skript - `master.R`
ausgeführt, die die Artikel lädt und in einer Iteration einen Dataframe erstellt. Aus diesem Dataframe wird schließlich ein .tsv-File erstellt: `data.tsv`. Dieses File ist die Wissensbasis für den Rasa-Bot.
Der Rasa Bot kann durch Custom-Actions zu einem _erkannten Intent_ und einer _erkannten physicist-Entität_ `data.tsv` durchsuchen und seinen Treffer als Antwort ausgeben. Damit wird der Bot zum Experten über voher festgelegte Themen zu den Physikern aus den gewonnenen Artikeln.

![](Wiki_Chatbot_Architecture.png)

# Ergebnisse

### Evaluation der Ergebnisse

![Recall](plots/feature_count_normal.png)

Mit den behandelten Intents ließen sich eine hohe Precision und ein geringer Recall erzielen.
Vor allem das Pattern-Matching bei 'spouse' liefert immer die richtige Information. Dennoch wird in ca. 90% der Fälle kein Ergebnis geliefert, da die nötige Information entweder _im Text fehlt_ oder über ein _anderes Pattern extrahiert_ werden müsste. In diesem Fall z.B. über `"* husband * *", "* wife * *"`; genutzt wird nur `"* marry * *"`.

Bei anderen Intents, die über NER und reguläre Ausdrücke extrahiert werden, ist die Precision geringer, da diese auf relativ naiven Angaben basieren ("Das erste Datum ist das Geburtsdatum"). Zudem ist die Qualität der gewonnenen Daten nicht sehr gut - oftmals sind sie sehr heterogen
```
  [1] "October 3, 1921"                    "23 January 1840"                   
  [3] "May 1960"                           "August 20, 1918"                   
  [5] "June 25, 1928"                      "December 4, 1913"                  
  [7] "November 30, 1939"                  "December 13, 1724"                 
  [9] "1976"                               "March 28, 1964"                    
 [11] "15 March 1930"                      "May 1908"           
```

```
 [64] "Stalin Prize, National Prize"                                                                  
 [65] "Nobel Price"
 [66] NA                                                 
 [67] NA
 [68] NA
 [69] "Nobel Price, Rumford Medal, Helmholtz Medal, Barnard Medal"
 [70] "Nobel Price"                              
 [71] NA                                             
 [72] "Scientific American Trophy for, Volta Prize, The Volta Prize, Albert Medal, Edison Medal, Alexander Graham Bell Medal"        
 [73] "Nobel Price"
 [74] "Nobel Price, Special Breakthrough Prize in Fundamental Physics"
 [75] NA
 [76] NA
```

* Bewertung RASA-Software
  - Setup schwierig
  - Keine Beispiel in Manuals, keine Dokumentation in Beispielen

* Bewertung Wikipedia-Artikel als unstrukturierte Daten als Grundlage
  - (+) relativ ähnlicher Aufbau
  - (-) dennoch im Detail unterschiede ("persönliche Note" ständig unterschiedlicher Autoren)
  - (-) unterschiedlich ausführlich

* Beantwortung der Forschungsfrage
> Kann man dieses Wissen aus Texten generieren?
  - Ja, nutze Pattern, NER
> Lässt sich dieses Wissen automatisch generieren?
  - Nein. Intents sollten vorher generiert werden, dann lässt sich das Wissen mit **auf den Intent und die Datengrundlage zugeschnittenen Verfahren** extrahieren