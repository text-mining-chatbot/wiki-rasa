### Structure of Bot Directory

```
├── Makefile                training and startup shortcuts for the bot, 'make train' and 'make run'
├── actions.py              Python script containing code for search and utterance actions
├── bot.py                  entry point for the bot
├── data.tsv                data source of the bot created by R script
├── domain.yml              entities, slots, actions of the bot
├── endpoints.yml           action server configuration
├── models                  Rasa subdirectory
├── nlu.md                  Rasa NLU training data with intents
├── nlu_config.yml          Rasa NLU configuration with NER model, tokenizer
└── stories.md              Rasa Core training data with example conversation flows
```
