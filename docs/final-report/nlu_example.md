## intent:nationality
- what nation is [Albert Einstein](physicist) from
- what nationality does [Albert Einstein](physicist) have
- where is [Galileo Galilei](physicist) from
