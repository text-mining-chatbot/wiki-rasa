% Projektplan Text Minig Praktikum WiSe 18/19 - Rasa Chatbot
% Lucas Schons, Lukas Gehrke, Jonas Wolff, Leonard Haas, David Fuhry
% 28.11.2018

## Gruppenmitglieder

*Name, Matrikel-Nummer*

- Jonas Wolff, 3720558
- Leonard Haas, 3755172
- David Fuhry, 3704472
- Lukas Gehrke, 3757499
- Lucas Schons, 3711400

## Aufgabenteilung Rasa-Chatbot

- __David Fuhry__
  - Beschaffung von Wikipedia-Daten zu Physikern
  - Weiterverarbeitung der Daten in R

- __Jonas Wolff__
  - Pflegen eines Rasa-Beispiel-Setups im repo
  - Auseinandersetzung mit Rasa-Slots für Erkennung von Namen in Intents

- __Leonard Haas__
  - Weiterverarbeitung der Daten in R; Fokus auf Cleaning

- __Lucas Schons__
  - Weiterverarbeitung der Daten in R; Fokus auf Named Entity Recognition mit dem Stanford NER

- __Lukas Gehrke__
  - Weiterverarbeitung der Daten in R; Fokus auf Extraktion von Geburtsdatum für Intent _Birth_
  - Beschäftigung mit Antwortgenerierung in Rasa Core

## Auflistung der Aufgaben und beabsichtigter Lösungsschritte

### Allgemeine Zielsetzung und Aufteilung

In dem Projekt soll die Frage geklärt werden, inwieweit Chatbots in der Lage sind, einerseits natürlichsprachliche Anfragen zu verstehen und andererseits Antworten aus einem natürlichsprachlichen Korpus zu generieren. Dazu soll sich mit der Open-Source Software [Rasa](https://rasa.com) zur Erstellung von Chatbots auseinandergesetzt werden.

Es soll am Beispiel eines englischsprachigen Chatbots, der Fragen zu berühmten Physikern beantworten kann, gearbeitet werden.

Die Aufgaben werden grob in zwei Bereiche aufgeteilt: Die Akquise und Extraktion von Daten und die Konfiguration des Rasa-Chatbots.

### Gewinnung von Physiker-Daten und Information Extraction

#### Zielsetzung

Für den Chatbot sollen Daten zu Physikern auf Englisch gewonnen werden. Als Quelle soll Wikipedia genutzt werden. Von dort wurde bereits eine Liste mit etwa [1000 Physikernamen](https://en.wikipedia.org/wiki/List_of_physicists) gewonnen. Zu den Namen sollen die Wikipedia-Artikel als HTML akquiriert werden.
Der so gewonnene Daten-Korpus soll dann in R für die Intents, mit denen der Chatbot arbeiten soll (siehe unten), aufbereitet werden.
Im Idealfall werden R-Scripts geschrieben, die fertige Rasa-Trainingsfiles ausgeben. 

#### Aufgaben

- Englischsprachige Wikipedia-Einträge zu berühmten Physikern als Korpus speichern
- Die Daten bereinigen (Entfernung von Tags, Hyperlinks etc)
- Für die Intents Skripte erstellen, um jeweils benötigte Informationen zu extrahieren.
  - Dazu gegebenenfalls weitere Aufbereitung (Named Entity Recognition, Part of Speech Tagging, Tokenization)
- Gewonnene Informationen in ein zu Rasa kompatibles Datenformat bringen
- Automatisierungspotenziale in den oben genannten Aufgaben bestimmen
  - Rausfinden, inwieweit die Erzeugung von Trainingsdaten vom Bot selbst übernommen werden kann

#### Arbeitsstand

- Es sind bereits 983 Physikernamen gewonnen worden:

  ```{r}
  Jules Aarons
  Ernst Karl Abbe
  Derek Abbott
  Hasan Abdullayev
  Alexei Alexeyevich Abrikosov
  Robert Adler
  Stephen L. Adler
  Franz Aepinus
  ...
  ```

- Die Wikipedia-Artikel zu allen Namen sind in einem XML-File gespeichert.

  ```{r}
  <page>
    <title>Jules Aarons</title>
    <ns>0</ns>
    <id>48756809</id>
    <revision>
      <id>867484885</id>
      <parentid>866458180</parentid>
      <timestamp>2018-11-06T01:00:04Z</timestamp>
      <contributor>
        <username>MopTop</username>
        <id>5027336</id>
      </contributor>
      <comment>citations</comment>
      <model>wikitext</model>
      <format>text/x-wiki</format>
  ...
  ```

- Es wurde ein R-Skript geschrieben, das Namen und zugehörige Wikipedia-Artikel in einem `.csv` File speichert

  ```{r}
  library(xml2)
  
  data <- read_xml("../data/Wikipedia-20181120103842.xml")
  
  title.nodes <- xml_find_all(data, ".//title")
  
  titles <- sapply(title.nodes, xml_text)
  
  text.nodes <- xml_find_all(data, ".//text")
  
  texts <- sapply(text.nodes, xml_text)
  
  df.out <- data.frame(Title = titles,
                       Text = texts)
  
  saveRDS(df.out, "../data/texte.RDS")
  
  write.table(df.out, "../data/texte.csv")
  
  ```

### Konfiguration des Rasa-Chatbots

Anmerkung: Dieser Aufgabenbereich wird in der ersten Phase des Projekts (Dezember 2018) hinter die Datenakquise zurückgestellt. Der Bot soll zunächst so einfach wie möglich implementiert werden.

#### Zielsetzung

Es soll ein [Rasa Chatbot](https://rasa.com/docs/) entwickelt werden, der auf Englisch Fragen zu Physikern beantwortet. Dazu sollen die Rasa-Technologien Rasa NLU (Natural Language Understanding) und Rasa Core verwendet werden. 
Rasa NLU soll dafür genutzt werden, dass Rasa die Fragen und dahinterliegenden Intents (Absichten) eines Chatbot-Nutzers versteht. Rasa Core ist für die Antwortgenerierung zuständig und steuert den Gesprächsverlauf. 
Um beide Teile erfolgreich zu implementieren, soll sich mit der Dokumenation von Rasa auseinandergesetzt werden. Besonderes Augenmerk liegt auf der Einspeisung der aufbereitenten Physikerdaten in den Rasa-Bot.
Im Laufe des Projekts soll zunächst ein Chatbot erstellt werden, der durch die Teammitglieder trainiert wird und einfach Fragen beantworten kann. (5 Intents siehe unten). Im weiteren Verlauf des Projekts soll geklärt werden, inwieweit der Bot selbstständig Trainingsdaten aus dem Korpus erstellen kann. Dazu sollen zB Rasa Custom Actions über rasa_core_sdk oder die [Erstellung von Bot Antworten](https://rasa.com/docs/core/responses/) über einen [externen Server](https://rasa.com/docs/core/customactions/) in Betracht gezogen werden.

#### Aufgaben

- Lauffähige Version des Rasa-Bots erstellen
- Intents für den Bot ausformulieren
- Den Bot mit den erstellten Intents trainieren
- Slots in den Intents nutzen, damit der Bot zwischen verschiedenen Entitäten unterscheiden kann
- Den Bot mit aufbereiteten Physikerdaten trainieren
- später: Möglichkeiten zum selbstständigen Training des Bots sichten

#### Arbeitsstand

- Der Rasa Bot läuft auf den Notebooks aller Teilnehmer in `pip` oder `miniconda`-Umgebungen
- Für den Physiker-Prototypen sollen folgende Intents implementiert werden:
  - _birth_ - "Where and when was `$name` born?"
  - _isAlive_ - "Is `$name` still alive?"
  - _education_ - "Where did `$name` go to school?"
  - _researchArea_ - "What did `$name` discover?"
  - _hasNobelPrize_ - "Did `$name` win the Nobel Prize?"

## Architekturdiagramm

![Rasa Chatbot Architektur](Rasa-Chatbot-Architektur.png)

## Absehbare und mögliche Probleme sowie Lösungsansätze

### Problem 1: Daten sind nicht vorhanden

Nicht alle Wikipedia Seiten von Physikern enthalten alle gewünschten Daten.  
Beispielsweise könnte es sein, dass nicht immer das Geburtsjahr oder der Geburtsort bekannt sind.

__Lösung__  
Standardsätze für nicht vorhandene Daten, wie "X/Y ist nicht bekannt" verwenden.

### Problem 2: Aufsetzen von Rasa gibt Probleme

Da Rasa von Tensorflow abhängt und Tensorflow noch kein Python3.7 unterstützt ist Python3.6.6 die letzte unterstützte Version.

__Lösung__  
Die einfachste Lösung wäre es, Python auf Version 3.6.6 zu downgraden. Dies kann jedoch zu Problemen vor allem in Linux-Systemen führen, da Python für viele Systemprozesse benötigt wird.  
Besser ist es eine virtuelle Python Umgebung mit Programmen wie Conda zu erstellen. Damit kann gezielt Python3.6.6 mit allen benötigten Packages installiert werden, unter denen man dann Rasa laufen lassen kann.

## Zeitplan

### Phase 1 bis zum 30.11

- Recherche zu Rasa NLU und Core
- Korpus aus Artikeln zu Physikern erstellen
- Intents zu Physikern für den Bot formulieren
- RASA-Prototyp von der Website auf eigener Hardware zum Laufen bekommen

#### Phase 2 bis zum 14.12

- Strukturierte Daten zu den formulierten Intents erstellen
- Protoypen eines Physiker-Chatbots zu den Intents erstellen 
- Klassifaktions-Algorithmen auswählen

#### Phase 3 bis Anfang Februar

- Feedback einarbeiten
- Finale Version entwickeln des Bot-Prototypen entwickeln
- Forschungsfrage beantworten - Inwieweit Training des Bots mit natürlichsprachigem Korpus möglich? 