---
title: Rasa Chatbot Demo
separator: <!--s-->
verticalSeparator: <!--v-->
theme: solarized
revealOptions:
    transition: 'convex'
    controls: true
    slideNumber: true
    
---

### Wikipedia Fact Chatbot

David Fuhry, Lukas Gehrke, Leonard Haas, 

Lucas Schons, Jonas Wolff

<img src="chatbot.png" width="40%">

<!--s-->

### Inhalt

* Thema & Forschungsfrage
* Lösungsansatz
* Ergebnisse

<!--s-->

### Thema - RASA Chatbot

<section style="text-align: left;">
RASA ist ein Framework für die Nutzung von **Chatbots** im Bereich _Conversational AI_. Das System muss mit **Facts** befüllt werden, um Antworten generieren zu können.

_"The Rasa Stack is a set of open source machine learning tools for developers to create contextual chatbots and assistants."_

<!--s-->

### Thema - Architektur von RASA

<img src="plots/rasa_architecture.png" width="70%">
Note: Bild von Rasa NLU und Core Architektur erklären

<!--s-->

### Forschungsfrage

- _Kann man dieses Wissen aus Texten akquirieren?_  
- _Kann dies automatisch mit Textquellen durchgeführt werden?_  

Dies soll anhand der _Wikipedia-Einträge von Physikern_ erprobt werden

<!--s-->

### Inhalt

* ~~Thema & Forschungsfrage~~
* Lösungsansatz
* Ergebnisse

<!--s-->

### Lösungsansatz - RASA Bot konfigurierern

* Für Training eines Bots werden _entities_, _text_ und _intents_ benötigt
* Definition _intent_ aus [RASA Docs](https://rasa.com/docs/nlu/dataformat/):  
_"The intent is the intent that should be associated with the text."_
* Antworten über _Custom Actions_, dafür **Wissensbasis** benötigt

<!--s-->

### Lösungsansatz - RASA Bot konfigurierern

#### Aufstellen von Intents zu Physikern:
* awards
* birthdate
* birthplace
* spouse
* university
* area of research
* ...

<!--s-->

### Lösungsansatz - Datenquelle

* 982 englischsprachige Wikipedia-Artikel
* Scraping über jeweiligen Link mit `wikipediR`
* HTML-Formatierung
* Ca. 550 Wörter/Artikel
<img src="kurchatov.png" width="70%">

<!--s-->

### Lösungsansatz - Processing

```
├── packages.list
├── script
│   └── master.R
└── wikiproc
    ├── DESCRIPTION
    ├── NAMESPACE
    ├── R
    │   ├── get_awards.R
    │   ├── get_birthdate.R
    │   ├── get_birthplace.R
    │   ├── get_data.R
    │   ├── get_spouse.R
    │   └── get_university.R
    ├── man
    └── tests
```
Note: Zu jedem Intent ein R-Skript. Master lädt die Artikel, speichert sie in einem Dataframe und ruft für jeden Artikel das clean_html-Skript und die Intent-processing-Skripte auf. Die extrahierten Informationen werden in einem neuen data-frame gespeichert. Außerdem wird das .tsv für den Bot generiert

<!--s-->

### Lösungsansatz - Gesamtarchitektur

<img src="Wiki_Chatbot_Architecture.png" width="100%">
Note: Schaubild der Gesamtarchitektur einfügen, AUF JEDEN FALL mit docker-Wal

<!--s-->

### Lösungsansatz - Processing, Extraktion Intents
   
#### `R/get_awards.R`

* Annahme: Alle Auszeichnungen im Text werden von spacy getaggt
* Matching aller Entities eines Textes gegen Menge an Stichwörtern.

<!--s-->

### Lösungsansatz - Processing, Extraktion Intents

* Beispiel `R/get_spouse.R`
* Identifiziere Sätze über Schlüsselwort _marry_ (lemma)
* Nutze _Pattern_ auf _POS-Tags_
* Verifiziere Ergebnisse über Physikernamen sowie _NER-Entities_

<!--s-->

### Vorführung RASA - Bot

Note: I see what you did there

<!--s-->

### Inhalt

* ~~Thema & Forschungsfrage~~
* ~~Lösungsansatz~~
* Ergebnisse

<!--s-->

### Ergebnisse

##### Anzahl gewonnener Ergebnisse zu Intents; Rest ist `NA`
<img src="plots/feature_count_flip.png" width="50%">
Note: fancy plots mit precision und recall zu awards, birthdate und spouse

<!--s-->

### Ergebnisse

<img src="plots/precision_birthdate.png" width="70%">
Note: Die Auswertung erfolgte händisch über die ersten 300 Ergebnisse von get_birthdate.R mit den Ergebnissen der infobox als ground truth (auch wenn hier tlw NA steht). Als partial match wurden solch Ergebnisse gewertet, die sinnvolle Daten sind und bis auf das Fehlen des Tages mit dem Referenzwert aus der Infobox übereinstimmen (BSP: infobox: "3 May 1960"; get_birthdate: "May 1960"). Als full match wurden solche Ergebnisse gewertet, die sinnvolle Daten sind und als Zeitangabe gegenüber der infobox nicht weniger ausführlich sind (kein Fehlen von Tag etc)


<!--s-->

##### Auswertung zu 'get_spouse.R'

<img src="plots/spouse_eval.png" width="50%">
Note: Recall noch ausbaufähig über integration weiterer Pattern.

<!--s-->

### Bewertung Software/Datengrundlage I

#### 1. Rasa-Bot
* (+) NLU funktioniert in Rasa sehr gut
* (+) Frei konfigurierbares Skript (actions.py)
* (-) RASA-Software schwierig aufzusetzen 
* (-) RASA wurde kurz vor Projektbeginn stark umgestellt
Note: Tutorial, Dokumenation schlagartig veraltet; keine Dokumentation in Beispielen, keine Beispiele in der Dokumentation

<!--s-->

### Bewertung Software/Datengrundlage II

#### 2. Wikipedia Artikel

* (+) Relativ einheitlicher Aufbau (Einleitung, Werdegang etc.)
* (-) Unterschiedlich ausführlich, tlw. Fehlen von Daten

<!--s-->

### Beantwortung der Forschungsfrage

* Definieren der Intents für den Bot sollte vorab geschehen  
* Informationen zu Intents unterschiedlich schwierig aus Text zu extrahieren  
Note: Wäre möglich, aber nicht sinnvoll, denn Intents sind Grundlage für Funktionieren von Rasa-Architektur; Verfahren teilweise allgemein verwendbar, tlw. auch von der Domäne abhängig

<!--s-->

### Ergebnisse

<section style="text-align: left;">
_Kann man dieses Wissen aus Texten akquirieren?_  
_Kann man dies automatisch mit Textquellen durchführen?_  

Es ist möglich, Fakten zu _vordefinierten Intents_ aus Texten zu extrahieren und diese _dem Bot zur Verfügung zu stellen_.

<!--s-->
