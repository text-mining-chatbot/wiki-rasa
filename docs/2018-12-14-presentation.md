# Rasa Chatbot

---

## Inhalt

* Zielsetzung
* Daten
* Status Quo
* Chatbot
* Ausblick

---

### Zielsetzung

RASA ist ein Framework für die Nutzung von Chatbots. Das System muss mit Facts befüllt
werden, um Antworten generieren zu können.  

- _Kann man dieses Wissen aus Texten akquirieren?_  
- _Kann dies automatisch mit Textquellen durchgeführt werden?_  

Dies soll anhand der Wikipedia-Einträge von Physikern erprobt werden.

---

### Beispielfragen

* Wer war Albert Einstein?
* Was hat Albert Einstein erfunden?
* Wo hat er gelebt?
* Wann ist er gestorben?
* ...

---

### Daten

* Knapp 1000 Wikipedia Artikel
* Über Wikipedia API
* HTML-Formatierung
* Ca. 550 Wörter/Artikel 
	* Großteil um 400 Wörter

---

## Funktionsweise Rasa-Bots

```
rasa
├── actions.py       - Custom Actions
├── domain.yml       - Entitäten, Antwortvorlagen
├── nlu.md           - Intents, Fragebeispiele
├── nlu_config.yml   - Pipeline
└── stories.md       - Gesprächsverläufe
```
---

## Datengrundlage als .tsv File 
 
```csv
name	birthplace	birthdate	day_of_death	place_of_death	is_alive  ...							
Albert Einstein	Ulm	14.03.1879	18.04.1955	Princeton	False ...							
Galileo Galilei	Pisa	15.02.1564	08.01.1642	Arcetri	False ...							
```

---

## Actions zur Antwortgenerierung

```python
class ActionSearchIsAlive(Action):
    def run(self, dispatcher, tracker, domain):
        import csv
        person = tracker.get_slot('physicist')
        with open('data.tsv') as csvfile:
            # search data.tsv

        return [SlotSet('is_alive', actual_is_alive)]
```
---

## NLU des Bots

```bash
"is Albert Einstein still alive"

2018-12-12 14:39:38 DEBUG    rasa_core.processor  - 
Received user message 'is Albert Einstein still alive'
 with intent '{'name': 'is_alive', 'confidence':
 0.9826370477676392}' and entities '[{'start': 
 3, 'end': 18, 'value': 'Albert Einstein', 
 'entity': 'Physicist', 'confidence': 0.9928692094590056, 
 'extractor': 'ner_crf'}]'
```

---

## Antworten des Bots

```bash
2018-12-12 18:54:23 DEBUG    rasa_core.processor  - 
Predicted next action 'action_search_is_alive' 
with prob 1.00. 2018-12-12 18:54:23 DEBUG    
rasa_core.processor  - Action 'action_search_is_alive'
ended with events '['SlotSet(key: is_alive, value: False)']'

"The Life status of Albert Einstein is False."
```

---

## Ausblick

* Information Extraction
    * Name-Entity-Recognition für "einfache" Intents: Orte, Datum etc.
    * word2vec oder Bootstrapping Relationship Extractors für die "schwierigeren"
      Intents: 'Forschungsgebiet' bzw. 'verheiratetMit', 'kindVon' etc.
* RASA trainieren
* APIs um die einzelnen Teile zusammen zu bringen

